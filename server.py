import socket as s
import json
from datetime import datetime

HOST = '127.0.0.1'
PORT = 65432

info = 'version: 0.1.0; creation date: 17.11.2022r'
start_time = datetime.now()

def help():
    msg = {
	"uptime": "returns the lifetime of the server",
	"info": "returns the version of the server, the date of its creation",
	"help": "returns a list of available commands",
	"stop": "stops server and client"
    }
    return json.dumps(msg, indent= 1)


def uptime():
    return json.dumps(str(datetime.now() - start_time))


server_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
server_socket.bind((HOST,PORT))
server_socket.listen()

conn, addr = server_socket.accept()
with conn:
    print(f"Connected by {addr}")

    while True:
        data = conn.recv(1024).decode('utf8')

        if data =='uptime':
            conn.send(uptime().encode('utf8'))
        if data =='info':
            conn.send(info.encode('utf8'))
        if data =='help':
            conn.send(help().encode('utf8'))
        if data =='stop':
            conn.send(('server closed').encode('utf8'))
            server_socket.close()
            break